#include "wrand.h"

int find_top(int *arr, int rand_num, int low, int high) {
    int mid;
    while(low < high) {
        mid = low + ((high - low) >> 1); /* (low+high)/2 */
        (rand_num > arr[mid]) ? (low = mid + 1) : (high = mid);
    }
    return (arr[low] >= rand_num) ? low : -1;
} 

int weighted_rand(int *arr, int *freq, int arr_size) {
    int prefix[arr_size];
    prefix[0] = freq[0];
    for (int i = 1; i < arr_size; ++i)
        prefix[i] = prefix[i-1] +  freq[i];
    int rand_num = rand() % prefix[arr_size-1] + 1;
    int top_index = find_top(prefix, rand_num, 0, arr_size-1);
    return arr[top_index];
}