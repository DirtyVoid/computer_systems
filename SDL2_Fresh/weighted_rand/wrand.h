#ifndef WRAND_H
#define WRAND_H

#include <stdlib.h>
#include <time.h>

extern int find_top(int *arr, int rand_num, int low, int high);
extern int weighted_rand(int *arr, int *freq, int arr_size);

#endif /* WRAND_H */