#include "main.h"

void sdlapp_init(sdlapp *app) {
    app->window = NULL;
    app->window_surface = NULL;
    app->cover_surface = NULL;
    app->emulator_surface = NULL;
    app->grenderer = NULL;
    app->gtexture = NULL;
    app->building.w = building_width;
    app->building.h = building_height;
    app->taxi_park.w = taxi_park_width;
    app->taxi_park.h = taxi_park_height;
    app->taxi_park.x = taxi_park_pos_x;
    app->taxi_park.y = taxi_park_pos_y;
    app->taxi_park_entrance.w = taxi_park_entrance_width;
    app->taxi_park_entrance.h = taxi_park_entrance_height;
    app->taxi_park_entrance.x = taxi_park_entrance_pos_x;
    app->taxi_park_entrance.y = taxi_park_entrance_pos_y;
}

void sdl_init_video(void) {
    rt_assert( (SDL_Init(SDL_INIT_VIDEO) >= 0), SDL_GetError(), );
}

void sdl_init_window(SDL_Window **window, const char *restrict window_name) {
    *window = SDL_CreateWindow( window_name, \
	    SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 
		WINDOW_WIDTH, WINDOW_HEIGHT, SDL_WINDOW_SHOWN
	);
    rt_assert(*window, SDL_GetError(), SDL_Quit());
}

void sdl_load_optimized_BMP(SDL_Surface **surface,const char *restrict bmp_filename) {
    SDL_Surface *temp_surface = SDL_LoadBMP(bmp_filename);
    sdlapp * app = container_of(surface, sdlapp, cover_surface);
    rt_assert(temp_surface, SDL_GetError(), sdl_close(&app->window));
    *surface = SDL_ConvertSurface(temp_surface, app->window_surface->format, 0);
    SDL_FreeSurface(temp_surface);
    rt_assert(*surface, SDL_GetError(), sdl_close(&app->window));
}

void sdl_init_renderer(SDL_Renderer **renderer) {
    rt_assert(SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "linear"), "Failed to set linear texture filtering",);
    sdlapp * app = container_of(renderer, sdlapp, grenderer);
    *renderer = SDL_CreateRenderer(app->window, -1, SDL_RENDERER_ACCELERATED);
    rt_assert(*renderer, SDL_GetError(), sdl_close(&app->window));
    
}

void sdl_renderer_fill_white(SDL_Renderer **renderer) {
    SDL_SetRenderDrawColor(*renderer, 0xFF, 0xFF, 0xFF, 0xFF);
    SDL_RenderClear(*renderer);
}

void sdl_rect_fill_color(SDL_Renderer **renderer, SDL_Rect *rect,
const int red, const int green, const int blue, const int alpha) {
    SDL_SetRenderDrawColor(*renderer, red, green, blue, alpha);
    SDL_RenderFillRect(*renderer, rect);
}


void sdl_init_image(sdlapp *app){
    int img_flags = IMG_INIT_PNG | IMG_INIT_JPG;
    rt_assert( IMG_Init (img_flags) & img_flags, IMG_GetError(), \
    (
        SDL_DestroyRenderer(app->grenderer),
        sdl_close(&app->window)
    ));
}

void sdl_load_texture_from_surface(SDL_Texture **texture,const char *restrict texture_filename) {
    if (*texture) SDL_DestroyTexture(*texture);
    sdlapp * app = container_of(texture, sdlapp, gtexture);
    SDL_Surface * temp_surface = IMG_Load(texture_filename);
    rt_assert(temp_surface, IMG_GetError(),);
    *texture = SDL_CreateTextureFromSurface(app->grenderer, temp_surface);
    rt_assert(*texture, SDL_GetError(),);
    SDL_FreeSurface(temp_surface);
}

void sdl_close(SDL_Window **window) {
    SDL_DestroyWindow(*window);
    IMG_Quit();
    SDL_Quit();
}