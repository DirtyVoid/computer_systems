#ifndef SDL2_SRC_H
#define SDL2_SRC_H

typedef struct __sdlapp {
    SDL_Window   *window;
    SDL_Surface  *window_surface;
    SDL_Surface  *cover_surface;
    SDL_Surface  *emulator_surface;
    SDL_Renderer *grenderer;
    SDL_Texture  *gtexture;
    SDL_Rect      building;
    SDL_Rect      taxi_park;
    SDL_Rect      taxi_park_entrance;
}sdlapp;



static const int WINDOW_HEIGHT = 480;
static const int WINDOW_WIDTH  = 640;

extern void sdlapp_init(sdlapp * app);
extern void sdl_init_video(void);
extern void sdl_init_window(SDL_Window **window, const char *restrict window_name);
extern void sdl_close(SDL_Window **window);
extern void sdl_load_optimized_BMP(SDL_Surface **surface,const char *restrict bmp_filename);
extern void sdl_init_renderer(SDL_Renderer **renderer);
extern void sdl_renderer_fill_white(SDL_Renderer **renderer);
extern void sdl_rect_fill_color(SDL_Renderer **renderer, SDL_Rect *rect, const int red, const int green, const int blue, const int alpha);
extern void sdl_init_image(sdlapp *app);
extern void sdl_load_texture_from_surface(SDL_Texture **texture,const char *restrict texture_filename);
#endif /* SDL2_SRC_H */
