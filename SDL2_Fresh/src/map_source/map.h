#ifndef S_MAP_H
#define S_MAP_H

#define BLOCK_WIDTH  64
#define BLOCK_HEIGHT 64

/* Note the building is a rectangle that has area = building_width * building_height
 * and the rest of the emuns signify the 8 bit rgb color of the rectangle.
 * In this case the rectangle is red. 
 */
enum building{
    building_width = BLOCK_WIDTH,
    building_height = BLOCK_HEIGHT,
};

enum eight_bit_rgb_color {
    red,
    green,
    blue,
    alpha
};

enum street {
    street_width = BLOCK_WIDTH,
    street_height = BLOCK_HEIGHT,
};

enum taxi_park {
    taxi_park_width=4*BLOCK_WIDTH,
    taxi_park_height=3*BLOCK_HEIGHT,
    taxi_park_pos_x=1664,
    taxi_park_pos_y=896,
    taxi_park_red=0,
    taxi_park_green = 0,
    taxi_park_blue = 0,
    taxi_park_alpha = 255,
    taxi_park_entrance_pos_x = 1664,
    taxi_park_entrance_pos_y = 960,
    taxi_park_entrance_width = BLOCK_WIDTH,
    taxi_park_entrance_height = BLOCK_HEIGHT,
    taxi_park_entrance_red=255,
    taxi_park_entrance_green = 255,
    taxi_park_entrance_blue = 255,
    taxi_park_entrance_alpha = 255, 
};

extern void draw_buildings(sdlapp *app, int seed);
extern void draw_taxi_park(sdlapp *app);


#endif /* S_MAP_H */