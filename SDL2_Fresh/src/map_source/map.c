#include "main.h"

#define x_pos_start 0
#define y_pos_start 0

void draw_buildings(sdlapp *app, int seed) {
    srand(seed);
    int color[] = {0,0,0,255};
    int arr[] = {red,green,blue};
    int freq[] = {60,30,10};
    for (int x = x_pos_start; x < WINDOW_WIDTH; x += street_width+building_width) {
        for (int y = y_pos_start; y < WINDOW_HEIGHT; y += street_height+building_height) {
            app->building.x = x;
            app->building.y = y;
            int rand_result = weighted_rand(arr,freq,3);
            color[rand_result] = 255;
            sdl_rect_fill_color(&app->grenderer, &app->building, color[red], color[green], color[blue], color[alpha]);
            color[rand_result] = 0;
        }
    }

}

void draw_taxi_park(sdlapp *app) {
    sdl_rect_fill_color(&app->grenderer, &app->taxi_park, taxi_park_red, taxi_park_green, taxi_park_blue, taxi_park_alpha);
    sdl_rect_fill_color(&app->grenderer, &app->taxi_park_entrance, taxi_park_entrance_red, taxi_park_entrance_green, taxi_park_entrance_blue, taxi_park_entrance_alpha);
}
