#include "main.h"
#define SEED 10
//#define TESTING

#ifndef TESTING
int main(void) {

    sdlapp app;
    memset(&app, 0, sizeof(app));
    sdlapp_init(&app);

    sdl_init_video();
    sdl_init_window(&app.window,"Taxi Emulator");
    sdl_init_renderer(&app.grenderer);
    sdl_init_image(&app);

    app.window_surface = SDL_GetWindowSurface(app.window);
    
    //
    
    //sdl_load_texture_from_surface(&app.gtexture,"assets/yellow-cabs-set/7943.jpg");
    //SDL_RenderCopy(app.grenderer,app.gtexture,NULL,NULL);
    // sdl_load_optimized_BMP(&app.cover_surface, "assets/cover.bmp");
    // SDL_Rect stretchRect = {.x = 0, .y = 0, .w = WINDOW_WIDTH, .h = WINDOW_HEIGHT};
	// SDL_BlitScaled(app.cover_surface, NULL, app.window_surface, &stretchRect);
	// SDL_UpdateWindowSurface( app.window );   
    // SDL_Delay(2000);
    
    SDL_Event e;
    bool quit = false;
    while (!quit) {
        while (SDL_PollEvent(&e)) {
            if( e.type == SDL_QUIT ) quit = true;
        }
        sdl_renderer_fill_white(&app.grenderer);
        draw_buildings(&app, SEED);
        draw_taxi_park(&app);
        SDL_RenderPresent(app.grenderer);
        
    }
    
    //SDL_FreeSurface( app.cover_surface );
    //SDL_DestroyTexture(app.gtexture);
    SDL_DestroyRenderer(app.grenderer);
    sdl_close(&app.window);
    return EXIT_SUCCESS;
}
#endif


#ifdef TESTING



int main()
{
    int arr[] = {0,1};
    int freq[]  = {30,70};
    int n = sizeof(arr) / sizeof(arr[0]);
    
    

    for (int i = 0; i <10; i++)
        printf("%d\n", weighted_rand(arr,freq,n));

    return 0;
}
 
#endif