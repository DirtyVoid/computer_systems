#ifndef MAIN_H
#define MAIN_H

/* Include standard library */
#include <stdbool.h>
#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <assert.h>
#include <ctype.h>
/* Include external library package */
#include <SDL.h>
#include <SDL_image.h>
/* Include macro library */
#include "container_of.h"
#include "runtime_assert.h"
/* Include subsystem headers */
#include "wrand.h"
#include "sdl2_src.h"
#include "map.h"



#endif
