#include "imp_sdl.h"
#include "container_of.h"
#include "runtime_assert.h"

int main (void) {

// FILE *log_file = fopen("log.txt", "w+");
//     if (!log_file) {
//         printf("Failed to create/open log.txt\n");
//         getchar();
//         exit(EXIT_FAILURE);
//     }

sdl_app app;
// sdl_app_null_init(&app);

sdl_window_init(&app.window);
// sdl_image_init();

app.window_surface = SDL_GetWindowSurface(app.window);
load_optimized_BMP_on_surface(&app.image_surface, "lib_imp_sdl/hw.bmp");

// int i = 1;
// rt_assert(i==0, log_file, "Not Equal", \
// 	(
// 		printf("Not Equal\n"),
// 		printf("Yes Equal\n")
// 	)
// );
	

SDL_Event e; 
bool quit = false; 
while(!quit) { 
	while( SDL_PollEvent( &e ) ) { 
		if( e.type == SDL_QUIT ) quit = true; 
	}
	SDL_Rect stretchRect = {.x = 0, .y = 0, .w = 1000, .h = 1000};
	SDL_BlitScaled(app.image_surface, NULL, app.window_surface, &stretchRect);
	SDL_UpdateWindowSurface( app.window );
}

SDL_Delay(1000);
SDL_FreeSurface( app.image_surface );
sdl_close(&app.window);
//fclose(log_file);
return EXIT_SUCCESS;



}
