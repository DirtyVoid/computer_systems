#ifndef RT_ASSERT_H
#define RT_ASSERT_H

#include <stdlib.h>
#include <stdio.h>

#ifndef __STRING
#define __STRING(x) #x
#endif

#define rt_assert(expr, file, str, ...)                                                                                     \
  ((void) sizeof ((expr) ? 1 : 0), __extension__ ({                                                                         \
    if (!(expr)) {                                                                                                          \
        fprintf((file), "In file %s:%d, in function: %s, assert failed: (%s). ",__FILE__, __LINE__, __func__, (#expr));     \
        fprintf((file), "Error: %s.\n",(str));                                                                              \
        __VA_ARGS__;                                                                                                        \
        fclose(file);                                                                                                       \
        exit(EXIT_FAILURE);                                                                                                 \
    }                                                                                                                       \
    }))
#endif
