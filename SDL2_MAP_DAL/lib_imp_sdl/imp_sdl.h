#ifndef IMP_SDL2_H
#define IMP_SDL2_H
#include <SDL.h>
#include <SDL_image.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>

typedef struct __sdl_app {
    SDL_Window *window;
    SDL_Surface *window_surface;
    SDL_Surface *image_surface;
}sdl_app;

static const int WINDOW_HEIGHT = 1920;
static const int WINDOW_WIDTH  = 1080;


extern void sdl_window_init(SDL_Window **window);
extern void sdl_close(SDL_Window **window);
extern void load_optimized_BMP_on_surface(SDL_Surface **surface, const char *restrict bmp_filename);
//extern void sdl_image_init(void);

static inline void sdl_app_null_init(sdl_app *app) {
    app->window = NULL;
    app->window_surface = NULL;
    app->image_surface = NULL;
}


#endif /* IMP_SDL2_H */