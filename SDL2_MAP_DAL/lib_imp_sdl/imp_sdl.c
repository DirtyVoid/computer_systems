#include "imp_sdl.h"
#include "container_of.h"
#include "runtime_assert.h"

void sdl_window_init(SDL_Window **window) {

    if ( SDL_Init( SDL_INIT_VIDEO ) < 0 ) {
        printf("Failed to initialize: %s\n", SDL_GetError());
        exit(EXIT_FAILURE);
    }

    *window = SDL_CreateWindow( "Taxi Emulator", \
		SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 
		WINDOW_HEIGHT, WINDOW_WIDTH, SDL_WINDOW_SHOWN
	);

    if(!*window) {
        printf("Failed to create window: %s\n", SDL_GetError());
        SDL_Quit();
        exit(EXIT_FAILURE);
    }

}

// void sdl_image_init(void) {
//     if(!(IMG_Init( IMG_INIT_PNG ) & IMG_INIT_PNG)) {
//         printf("Failed to start SDL_image: %s\n", IMG_GetError());
//         SDL_Quit();
//         exit(EXIT_FAILURE);
//     }
// }


void load_optimized_BMP_on_surface(SDL_Surface **surface, const char *restrict bmp_filename) {
    SDL_Surface *surface_to_load = SDL_LoadBMP(bmp_filename);
    sdl_app * app = container_of(surface, sdl_app, image_surface);
    if(!surface_to_load) {
        printf("Failed to create image: %s\n", SDL_GetError());
        sdl_close(&app->window);
        exit(EXIT_FAILURE);
    }
    *surface = SDL_ConvertSurface(surface_to_load, app->window_surface->format, 0);
    SDL_FreeSurface(surface_to_load);
    if(!*surface) {
        printf("Failed to create image: %s\n", SDL_GetError());
        sdl_close(&app->window);
        exit(EXIT_FAILURE);
    }
}

void sdl_close(SDL_Window **window) {
    SDL_DestroyWindow( *window );
    SDL_Quit();
}